#include "globals.h"

#include "floatseq.h"
#include "intset.h"

#include <ctime>
#include <fstream>
#include <iostream>
#include <unistd.h>
#include <utility>
#include <vector>

namespace std
{
// inject intset.h operators into std
using BATLibGen::operator>>;
using BATLibGen::operator<<;

std::istream &operator>>(std::istream &in, std::pair<unsigned int, unsigned int> &atom_pair)
{
    std::ios_base::fmtflags old_flags = in.setf(std::ios_base::skipws);
    in >> atom_pair.first >> atom_pair.second;
    in.flags(old_flags);
    return in;
}

}

namespace BATLibGen
{

Globals globals;

namespace po = boost::program_options;

Globals::Globals()
{
    opts.add_options()
        ("input,i", po::value<std::string>()->required(), "Input file containing the starting structure")
        ("output,o", po::value<std::string>()->required(), "Output .xyz file for the generated library")
        ("multifile,m", po::bool_switch(), "Output structures to separate numbered files")
        ("align,a", po::bool_switch(), "Align all structures to the starting one")
        ("clash-threshold", po::value<float>()->default_value(1.5), "Remove structures with atoms closer than threshold")
        ("bonded-clash-threshold", po::value<float>()->default_value(0.5), "Clash threshold for atoms sharing a bond")
        ("protonate", po::bool_switch(), "Generate structures protonated on basic atoms (instead of normal B/A/T structures)")
        ("bond-factors", po::value<FloatSeq<>>()->default_value(boost::lexical_cast<FloatSeq<>>("0.8..1.5,1.6..2..0.2")), "Bond scaling factors")
        ("angle-factors", po::value<FloatSeq<>>()->default_value(boost::lexical_cast<FloatSeq<>>("0.8..0.95..0.05,1.05..1.2..0.05")), "Valence angle scaling factors")
        ("torsion-offsets", po::value<FloatSeq<>>()->default_value(boost::lexical_cast<FloatSeq<>>("10..350")), "Torsion angle offsets")
        ("outofplane-offsets", po::value<FloatSeq<>>()->default_value(boost::lexical_cast<FloatSeq<>>("-0.2..-0.05..0.05,0.05..0.2..0.05")), "Out-of-plane offsets")
        ("nbdist-factors", po::value<FloatSeq<>>()->default_value(FloatSeq<>()), "Nonbonded distance scaling factors")
        ("nbpair-cutoff", po::value<float>()->default_value(0.5), "Nonbonded pair weighting cutoff (added to minimum pair distance)")
        ("active-atoms", po::value<std::set<unsigned int>>()->default_value(std::set<unsigned int>(), std::string()), "Set of active atoms (other atoms are ignored)")
        ("add-bond", po::value<std::vector<std::pair<unsigned int, unsigned int>>>()->default_value(std::vector<std::pair<unsigned int, unsigned int>>(), ""), "Add a virtual bond between specified atoms")
        ("config", po::value<std::string>()->default_value("batlibgen.cfg"), "Configuration file")
        ("help", po::bool_switch(), "Print help and exit")
        ;
}

bool Globals::init_config(int argc, char *argv[])
{
    try {
        po::store(po::parse_command_line(argc, argv, opts), optmap);

        if (optmap["help"].as<bool>()) {
            print_help(opts);
            return false;
        }

        std::ifstream cfgfile(optmap["config"].as<std::string>());
        if (cfgfile.good()) {
            po::store(po::parse_config_file(cfgfile, opts), optmap);
        }

        po::notify(optmap);
    } catch (boost::program_options::error &e) {
        std::cerr << "Error parsing options: " << e.what() << '\n' << std::endl;
        print_help(opts);
        return false;
    }

    return true;
}

void Globals::print_help(const boost::program_options::options_description &opt_desc) const
{
    std::cerr << "Generate a library of conformations by altering all Bond/Angle/Torsion coordinates" << std::endl;
    std::cerr << opt_desc << std::endl;
}

}
