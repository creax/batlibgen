#ifndef BATLIBGEN_FLOATSEQ_H
#define BATLIBGEN_FLOATSEQ_H

#include <boost/operators.hpp>
#include <boost/program_options.hpp>
#include <boost/tokenizer.hpp>

#include <cctype>
#include <cmath>
#include <cstddef>
#include <istream>
#include <ostream>
#include <string>
#include <vector>

namespace BATLibGen {

class RangeTokenizerFunc
{
public:
    typedef std::string Token;
    typedef std::string::const_iterator Iterator;
    bool operator()(Iterator &next, Iterator end, Token &tok)
    {
        tok.clear();

        bool in_doubledot = false;
        for (; next != end; ++next) {
            if (in_doubledot) {
                if (*next == '.') {
                    ++next;
                    if (tok.empty()) {
                        in_doubledot = false;
                        continue;
                    } else {
                        break;
                    }
                } else {
                    tok += '.';
                    in_doubledot = false;
                }
            }
            if (std::isspace(*next)) {
                continue;
            } else if (*next == '.') {
                in_doubledot = true;
                continue;
            }

            tok += *next;
        }

        return !tok.empty();
    };
    void reset()
    {
    };
};


typedef boost::token_iterator<RangeTokenizerFunc, RangeTokenizerFunc::Iterator, RangeTokenizerFunc::Token> RangeTokenIterator;


template<typename I> class Decimal;
template<typename I> std::istream &operator>>(std::istream &in, Decimal<I> &dec);

template<typename I> class Decimal
    : private boost::totally_ordered<Decimal<I>>
    , private boost::totally_ordered<Decimal<I>, int>
    , private boost::additive<Decimal<I>>
{
public:
    Decimal(void) : coeff(0), multiplier(1)
    {
    }

    Decimal(const Decimal<I> &d) : coeff(d.coeff), multiplier(d.multiplier)
    {
    }

    template<typename F> F real(void) const
    {
        return static_cast<double>(coeff)/static_cast<double>(multiplier);
    }

    friend std::istream &operator>><>(std::istream &in, Decimal<I> &dec);

    bool operator<(const Decimal<I> &d) const
    {
        if (multiplier == d.multiplier) {
            return coeff < d.coeff;
        }
        else if (coeff >= 0 && d.coeff < 0) {
            return false;
        }
        else {
            return (coeffcmp(*this, d) < 0);
        }
    }

    bool operator==(const Decimal<I> &d) const
    {
        if (multiplier == d.multiplier) {
            return coeff == d.coeff;
        }
        else if ((coeff >= 0 && d.coeff < 0) || (coeff < 0 && d.coeff >= 0)) {
            return false;
        }
        else {
            return (coeffcmp(*this, d) == 0);
        }
    }

    bool operator<(int x) const
    {
        if ((multiplier == 1) || (x == 0)) {
            return coeff < x;
        }
        else if (coeff >= 0 && x < 0) {
            return false;
        }
        else {
            return (coeffcmp(*this, Decimal(x, 1)) < 0);
        }
    }

    bool operator>(int x) const
    {
        if ((multiplier == 1) || (x == 0)) {
            return coeff > x;
        }
        else if (coeff >= 0 && x < 0) {
            return true;
        }
        else {
            return (coeffcmp(*this, Decimal(x, 1)) > 0);
        }
    }

    bool operator==(int x) const
    {
        if ((multiplier == 1) || (x == 0)) {
            return coeff == x;
        }
        else if ((coeff >= 0 && x < 0) || (coeff <= 0 && x > 0)) {
            return false;
        }
        else {
            return (coeffcmp(*this, Decimal(x, 1)) == 0);
        }
    }

    Decimal &operator+=(Decimal<I> rhs)
    {
        while (multiplier < rhs.multiplier) {
            multiplier *= 10;
            coeff *= 10;
        }
        while (rhs.multiplier < multiplier) {
            rhs.multiplier *= 10;
            rhs.coeff *= 10;
        }

        coeff += rhs.coeff;
        return *this;
    }

    Decimal &operator-=(const Decimal<I> &rhs)
    {
        return *this += (-rhs);
    }

    Decimal operator-(void) const
    {
        return Decimal(-coeff, multiplier);
    }

    Decimal lsd(void) const
    {
        I lsd_coeff = 1;
        if ((multiplier == 1) && (coeff >= 10) && ((coeff % (10 * lsd_coeff)) == 0)) {
            while ((coeff % (10 * lsd_coeff)) == 0) {
                lsd_coeff *= 10;
            }
            return Decimal(lsd_coeff, 1);
        }
        else {
            return Decimal(1, multiplier);
        }
    }
protected:
    Decimal(I coeff, I multiplier) : coeff(coeff), multiplier(multiplier)
    {
    }

    static int coeffcmp(Decimal<I> a, Decimal<I> b)
    {
        while (a.multiplier < b.multiplier) {
            a.multiplier *= 10;
            a.coeff *= 10;
        }
        while (b.multiplier < a.multiplier) {
            b.multiplier *= 10;
            b.coeff *= 10;
        }

        if (a.coeff == b.coeff) {
            return 0;
        }
        else if (a.coeff < b.coeff) {
            return -1;
        }
        else {
            return 1;
        }
    }
private:
    I coeff, multiplier;
};


template<typename I> std::istream &operator>>(std::istream &in, Decimal<I> &dec)
{
    std::istream::sentry s(in);

    if (!s) {
        return in;
    }

    bool in_fract = false;
    bool negative = false;


    dec.multiplier = 1;
    dec.coeff = 0;

    std::streambuf *buf = in.rdbuf();
    std::streambuf::int_type c = buf->sgetc();

    if (c == '-') {
        negative = true;
        buf->sbumpc();
    }

    while ((c = buf->sbumpc()) != std::streambuf::traits_type::eof()) {
        if (c >= '0' && c <= '9') {
            if (in_fract) {
                dec.multiplier *= 10;
            }

            dec.coeff *= 10;
            dec.coeff += c - '0';
        }
        else if (!in_fract && c == '.') {
            in_fract = true;
        }
        else {
            in.setstate(std::ios_base::failbit);
            break;
        }
    }

    if (negative) {
        dec.coeff = -dec.coeff;
    }

    if (c == std::streambuf::traits_type::eof()) {
        in.setstate(std::ios_base::eofbit);
    }

    return in;
}


template<typename T> class FloatSeq;
template<typename T> std::istream &operator>>(std::istream &in, FloatSeq<T> &fs);
template<typename T> std::ostream &operator<<(std::ostream &out, const FloatSeq<T> &fs);

template<typename T = float> class FloatSeq : public std::vector<T>
{
public:
    friend std::istream &operator>><>(std::istream &in, FloatSeq<T> &fs);
    friend std::ostream &operator<<<>(std::ostream &out, const FloatSeq<T> &fs);
};


template<class T, class charT> void validate(boost::any &v, const std::vector<std::basic_string<charT>> &s, FloatSeq<T>*, int)
{
    boost::program_options::validate(v, s, (FloatSeq<T>*)0, 0L);
}


template<typename T> std::istream &operator>>(std::istream &in, FloatSeq<T> &fs)
{
    std::string element;
    Decimal<long> range_start, range_end, increment;
    std::istringstream str;
    str.exceptions(std::ios::failbit|std::ios::badbit);

    while (in.good()) {
        getline(in, element, ',');
        RangeTokenIterator tok(RangeTokenizerFunc(), element.begin(), element.end());

        if (tok.at_end()) {
            // error, no tokens to process here
            continue;
        }

        std::string start_str = *tok;
        str.str(start_str);
        str.clear();
        str >> range_start;
        if ((++tok).at_end()) {
            fs.push_back(range_start.real<T>());
            continue;
        }

        std::string end_str = *tok;
        str.str(end_str);
        str.clear();
        str >> range_end;
        if ((++tok).at_end()) {
            increment = std::min(range_start.lsd(), range_end.lsd());
        } else {
            str.str(*tok);
            str.clear();
            str >> increment;
        }

        if (increment == 0) {
            fs.push_back(range_start.real<T>());
            continue;
        }

        if (((range_start > range_end) && (increment > 0)) ||
            ((range_start < range_end) && (increment < 0))) {
            increment = -increment;
        }

        if (range_start > range_end) {
            for (Decimal<long> x = range_start; x >= range_end; x += increment) {
                fs.push_back(x.real<T>());
            }
        }
        else {
            for (Decimal<long> x = range_start; x <= range_end; x += increment) {
                fs.push_back(x.real<T>());
            }
        }
    }

    if (in.eof() && in.fail()) {
        // trying to getline from an empty stream sets both eof and fail
        // clear fail as that would cause Boost to throw an error
        in.clear(in.eofbit);
    }

    return in;
}


template<typename T> std::ostream &operator<<(std::ostream &out, const FloatSeq<T> &fs)
{
    bool first = true;
    for (T x : fs) {
        if (first) {
            first = false;
        } else {
            out << ',';
        }
        out << x;
    }
    return out;
}


}

#endif
