#include "fraglibgen_globals.h"

#include <fstream>
#include <iostream>
#include <vector>

namespace FragLibGen
{

Globals globals;

namespace po = boost::program_options;

Globals::Globals()
{
    opts.add_options()
        ("input,i", po::value<std::vector<std::string>>()->required(), "Input file containing the starting structure")
        ("output,o", po::value<std::string>()->default_value("frag-"), "Output file name prefix for the generated library")
        ("passes,p", po::value<unsigned int>()->default_value(0), "Number of recursive fragmentation passes")
        ("config", po::value<std::string>()->default_value("fraglibgen.cfg"), "Configuration file")
        ("help", po::bool_switch(), "Print help and exit")
        ;

    pos_opts.add("input", -1);
}

bool Globals::init_config(int argc, char *argv[])
{
    try {
        po::store(po::command_line_parser(argc, argv).options(opts).positional(pos_opts).run(), optmap);

        if (optmap["help"].as<bool>()) {
            print_help(opts);
            return false;
        }

        std::ifstream cfgfile(optmap["config"].as<std::string>());
        if (cfgfile.good()) {
            po::store(po::parse_config_file(cfgfile, opts), optmap);
        }

        po::notify(optmap);
    } catch (boost::program_options::error &e) {
        std::cerr << "Error parsing options: " << e.what() << '\n' << std::endl;
        print_help(opts);
        return false;
    }

    return true;
}

void Globals::print_help(const boost::program_options::options_description &opt_desc) const
{
    std::cerr << "Generate a library of radical fragments by cutting all bonds in input molecules" << std::endl;
    std::cerr << opt_desc << std::endl;
}

}
