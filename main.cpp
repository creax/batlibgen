#include "batlibrary.h"
#include "globals.h"

#include <openbabel/mol.h>
#include <openbabel/obconversion.h>

#include <exception>
#include <execinfo.h>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <utility>

namespace BATLibGen {
extern const char *version;

void terminate_handler(void)
{
    void *trace_addrs[20];
    int trace_len = backtrace(trace_addrs, sizeof(trace_addrs));
    backtrace_symbols_fd(trace_addrs, trace_len, 2);

    __gnu_cxx::__verbose_terminate_handler();
}

std::unique_ptr<OpenBabel::OBMol> read_molecule(const std::string filename)
{
    OpenBabel::OBConversion conv;
    OpenBabel::OBFormat *format = conv.FormatFromExt(filename);

    if (!format || !conv.SetInFormat(format)) {
        std::ostringstream s;
        s << "Unsupported file format: " << filename;
        throw std::runtime_error(s.str());
    }

    std::ifstream str(filename);
    if (!str.good()) {
        std::ostringstream s;
        s << "Can't open input file: " << filename;
        throw std::runtime_error(s.str());
    }

    std::unique_ptr<OpenBabel::OBMol> mol(new OpenBabel::OBMol);
    conv.Read(mol.get(), &str);

    return mol;
}

void add_bonds(OpenBabel::OBMol &mol)
{
    for (const std::pair<unsigned int, unsigned int> &atom_pair : globals.cfg<std::vector<std::pair<unsigned int, unsigned int>>>("add-bond")) {
        OpenBabel::OBAtom *a = mol.GetAtom(atom_pair.first), *b = mol.GetAtom(atom_pair.second);
        if (!a || !b) {
            throw std::invalid_argument("add_bonds: Invalid atom ID");
        }

        if (mol.GetBond(a, b)) {
            continue;
        }

        mol.AddBond(atom_pair.first, atom_pair.second, 1);
        std::cout << "# adding a bond between "
                  << OpenBabel::etab.GetSymbol(a->GetAtomicNum()) << atom_pair.first
                  << " and "
                  << OpenBabel::etab.GetSymbol(b->GetAtomicNum()) << atom_pair.second
                  << std::endl;
    }
}

}


int main(int argc, char *argv[])
{
    using namespace BATLibGen;

    std::set_terminate(terminate_handler);

    std::cout << "# BATLibGen version " << version << std::endl;

    if (!globals.init_config(argc, argv)) {
        return 1;
    }

    std::unique_ptr<OpenBabel::OBMol> initmol = read_molecule(globals.cfg<std::string>("input"));
    add_bonds(*initmol);

    BATLibrary lib(std::move(initmol));
    lib.set_active(globals.cfg<std::set<unsigned int>>("active-atoms"));

    if (globals.cfg<bool>("protonate")) {
        lib.generate_protonated_structures();
    } else {
        lib.generate_bond_structures();
        lib.generate_angle_structures();
        lib.generate_torsion_structures();
        lib.generate_outofplane_structures();
        lib.generate_nbdist_structures();
    }

    lib.remove_clashes();

    if (globals.cfg<bool>("align")) {
        lib.align();
    }

    OpenBabel::OBConversion out_conv;
    out_conv.SetOutFormat("xyz");
    if (globals.cfg<bool>("multifile")) {
        const std::string &prefix = globals.cfg<std::string>("output");
        std::ofstream listfile(prefix + ".batlib");
        unsigned int width = 1, N = lib.size();
        while (N >= 10) {
            N /= 10;
            width++;
        }
        unsigned int i = 0;
        for (auto &s : lib) {
            std::ostringstream ofn(prefix, std::ios_base::ate);
            ofn << '.' << std::setfill('0') << std::setw(width) << i++ << ".xyz";
            std::ofstream ofile(ofn.str());
            out_conv.Write(s.get(), &ofile);
            listfile << ofn.str() << '\n';
        }
    } else {
        std::ofstream ofile(globals.cfg<std::string>("output"));
        for (auto &s : lib) {
            out_conv.Write(s.get(), &ofile);
        }
    }

    return 0;
}
