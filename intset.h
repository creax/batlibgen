#ifndef BATLIBGEN_INTSET_H
#define BATLIBGEN_INTSET_H

#include <istream>
#include <ostream>
#include <set>

namespace BATLibGen
{

template<typename T> std::istream &operator>>(std::istream &in, std::set<T> &set)
{
    std::string element;
    T range_start, range_end;
    std::istringstream str;
    str.exceptions(std::ios::failbit|std::ios::badbit);

    while (in.good()) {
        getline(in, element, ',');
        if (element.size() == 0) {
            // error, no tokens to process here
            continue;
        }

        str.str(element);
        str.clear();

        str >> range_start;
        range_end = range_start;
        if (str.good() && (str.peek() == '-')) {
            str.ignore(1);
            str >> range_end;
        }

        for (T x = range_start; x <= range_end; x++) {
            set.insert(x);
        }
    }

    return in;
}


template<typename T> std::ostream &operator<<(std::ostream &out, const std::set<T> &set)
{
    bool first = true;
    for (T x : set) {
        if (first) {
            first = false;
        } else {
            out << ',';
        }
        out << x;
    }
    return out;
}

}

#endif
