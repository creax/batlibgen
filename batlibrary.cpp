#include "batlibrary.h"
#include "floatseq.h"
#include "globals.h"
#include "obmolextras.h"

#include <openbabel/obiter.h>

#include <iostream>
#include <sstream>
#include <streambuf>

namespace BATLibGen {

BATLibrary::BATLibrary(std::unique_ptr<OpenBabel::OBMol> &&input_mol) : mol(std::move(input_mol))
{
}


void BATLibrary::generate_bond_structures(void)
{
    for (OpenBabel::OBMolBondIter it(*mol); it; ++it) {
        std::array<unsigned int, 2> atoms = {it->GetBeginAtomIdx(), it->GetEndAtomIdx()};
        if (!check_active(atoms)) {
            continue;
        }
        for (float f : globals.cfg<FloatSeq<>>("bond-factors")) {
            push_back(displace_bond(*it, f));
        }
    }
}


void BATLibrary::generate_angle_structures(void)
{
    std::array<unsigned int, 3> corrected_idxs;
    for (OpenBabel::OBMolAngleIter it(*mol); it; ++it) {
        if (!check_active(*it)) {
            continue;
        }
        for (float f : globals.cfg<FloatSeq<>>("angle-factors")) {
            const std::vector<unsigned int> &orig_idxs = *it;
            // indices from OBMolAngleIter are zero-based and ordered B, A, C
            corrected_idxs = {{orig_idxs[1] + 1, orig_idxs[0] + 1, orig_idxs[2] + 1}};
            push_back(displace_angle(corrected_idxs, f));
        }
    }
}


void BATLibrary::generate_torsion_structures(void)
{
    for (OBUniqueTorsionIter it(*mol); it; ++it) {
        std::array<unsigned int, 2> main_atoms = {(*it)[1], (*it)[2]};
        if (!check_active(main_atoms)) {
            continue;
        }
        for (float x : globals.cfg<FloatSeq<>>("torsion-offsets")) {
            push_back(displace_torsion(*it, x));
        }
    }
}


void BATLibrary::generate_outofplane_structures(void)
{
    for (OBPlanarCenterIter it(*mol); it; ++it) {
        if (!check_active(*it)) {
            continue;
        }
        for (float x : globals.cfg<FloatSeq<>>("outofplane-offsets")) {
            push_back(displace_outofplane(*it, x));
        }
    }
}


void BATLibrary::generate_protonated_structures(void)
{
    for (OBBasicAtomIter it(*mol); it; ++it) {
        std::array<unsigned int, 1> atom = {it->GetIdx()};
        if (!check_active(atom)) {
            continue;
        }

        auto p = protonate_atom(*it);
        for (float f : globals.cfg<FloatSeq<>>("bond-factors")) {
            push_back(displace_bond(*p.first, *p.second, f));
        }
    }
}


void BATLibrary::generate_nbdist_structures(void)
{
    const FloatSeq<> &factors = globals.cfg<FloatSeq<>>("nbdist-factors");

    if (factors.empty()) {
        return;
    }

    std::vector<std::vector<unsigned int>> monomers = OBMolFindMonomers(*mol);

    if (monomers.size() <= 1) {
        return;
    }

    if (monomers.size() > 2) {
        throw std::runtime_error("BATLibrary::generate_nbdist_structures: only dimers implemented");
    }

    OpenBabel::vector3 c1, c2;

    if (!active_atoms.empty()) {
        std::vector<std::vector<unsigned int>> ref_atoms;
        for (auto &m : monomers) {
            ref_atoms.push_back(filter_active(m));
            if (ref_atoms.back().empty()) {
                throw std::runtime_error("BATLibrary::generate_nbdist_structures: monomer has no active atoms left");
            }
        }

        c1 = OBMolGroupCenter(*mol, ref_atoms[0]);
        c2 = OBMolGroupCenter(*mol, ref_atoms[1]);
    } else {
        double mindist, cutoff = globals.cfg<float>("nbpair-cutoff");
        auto nbpairs = OBMolNonbondedNeighbors(*mol, monomers[0], monomers[1], mindist, cutoff);
        std::vector<double> weights(mol->NumAtoms() + 1); // OB atom indices are one-based
        for (auto &x : nbpairs) {
            unsigned int i, j;
            double d;
            std::tie(i, j, d) = x;
            if (d < mindist + cutoff) {
                // linear ramp from 1 at mindist to 0 at mindist + cutoff
                double w = 1.0 - (d - mindist) / cutoff;
                weights[i] += w;
                weights[j] += w;
            }
        }

        c1 = OBMolGroupWeightedCenter(*mol, monomers[0], weights);
        c2 = OBMolGroupWeightedCenter(*mol, monomers[1], weights);
    }

    OpenBabel::vector3 dir = c2 - c1;
    double dist = dir.length();
    dir /= dist;

    for (float f : factors) {
        push_back(displace_nbdist(monomers[0], monomers[1], dir, dist, f));
    }
}


void BATLibrary::align(void)
{
    double *ref_coords = mol->GetCoordinates();
    unsigned int N = mol->NumAtoms();
    for (auto &m : *this) {
        double R[3][3];
        OpenBabel::qtrfit(ref_coords, m->GetCoordinates(), N, R);
        m->Rotate(R);
    }
}

/*
 * This is not very useful (or not at all), as it apparently
 * doesn't catch equivalent atoms (methyl rotations)
 */
void BATLibrary::remove_duplicates(void)
{
    std::vector<std::shared_ptr<OpenBabel::OBMol>> filtered;
    unsigned int N = mol->NumAtoms();

    for (auto it_ref = cbegin(); it_ref != cend(); ++it_ref) {
        double *ref_coords = (*it_ref)->GetCoordinates();
        double duplicate_found = false;
        for (auto it_test = cbegin(); it_test != it_ref; ++it_test) {
            double *test_coords = (*it_test)->GetCoordinates();
            double rms = OpenBabel::calc_rms(ref_coords, test_coords, N);
            if (rms < 0.01) {
                duplicate_found = true;
                break;
            }
        }
        if (!duplicate_found) {
            filtered.push_back(*it_ref);
        }
    }
    swap(filtered);
}


void BATLibrary::remove_clashes(void)
{
    float threshold = globals.cfg<float>("clash-threshold");
    float bonded_threshold = globals.cfg<float>("bonded-clash-threshold");
    std::vector<std::shared_ptr<OpenBabel::OBMol>> filtered;

    for (auto &m : *this) {
        double min_dist = OBMolMinPairDistance(*m, true);
        double min_bonded_dist = OBMolMinPairDistance(*m, false);
        if ((min_dist > threshold) && (min_bonded_dist > bonded_threshold)) {
            filtered.push_back(std::move(m));
        }
    }
    swap(filtered);
}


std::shared_ptr<OpenBabel::OBMol> BATLibrary::displace_bond(const OpenBabel::OBMol &mol, const OpenBabel::OBBond &bond, float factor)
{
    std::shared_ptr<OpenBabel::OBMol> out(new OpenBabel::OBMol(mol));
    OpenBabel::OBBond *new_bond = out->GetBond(bond.GetIdx());
    double new_length = factor * new_bond->GetLength();

    std::ostringstream label;
    label << "DIST " << bond.GetBeginAtomIdx() << " " << bond.GetEndAtomIdx() << " " << new_length;
    out->SetTitle(label.str().c_str()); // c_str() needed as SetTitle(std::string &) is unusable

    // silence unwanted unconditional debugging output from SetLength
    std::streambuf *stderr_buf = std::cerr.rdbuf(nullptr);
    new_bond->SetLength(new_length);
    std::cerr.rdbuf(stderr_buf);

    return out;
}


std::shared_ptr<OpenBabel::OBMol> BATLibrary::displace_angle(const std::array<unsigned int, 3> &angle_idxs, float factor)
{
    std::shared_ptr<OpenBabel::OBMol> out(new OpenBabel::OBMol(*mol));
    OpenBabel::OBAtom *a = out->GetAtom(angle_idxs[0]), *b = out->GetAtom(angle_idxs[1]), *c = out->GetAtom(angle_idxs[2]);
    double new_angle = normalize_angle(factor * a->GetAngle(b, c));

    std::ostringstream label;
    label << "ANGLE ";
    for (auto idx : angle_idxs) {
        label << idx << " ";
    }
    label << new_angle;
    out->SetTitle(label.str().c_str()); // c_str() needed as SetTitle(std::string &) is unusable

    OBMolSetAngleSymmetric(*out, a, b, c, new_angle);

    return out;
}


std::shared_ptr<OpenBabel::OBMol> BATLibrary::displace_torsion(const std::array<unsigned int, 4> &tors_idxs, float x)
{
    std::shared_ptr<OpenBabel::OBMol> out(new OpenBabel::OBMol(*mol));
    double new_angle = normalize_angle(x + mol->GetTorsion(tors_idxs[0], tors_idxs[1], tors_idxs[2], tors_idxs[3]));

    std::ostringstream label;
    label << "DIHED ";
    for (auto idx : tors_idxs) {
        label << idx << " ";
    }
    label << new_angle;
    out->SetTitle(label.str().c_str()); // c_str() needed as SetTitle(std::string &) is unusable

    out->SetTorsion(out->GetAtom(tors_idxs[0]), out->GetAtom(tors_idxs[1]),
                    out->GetAtom(tors_idxs[2]), out->GetAtom(tors_idxs[3]), new_angle * DEG_TO_RAD);

    return out;
}


std::shared_ptr<OpenBabel::OBMol> BATLibrary::displace_outofplane(const std::array<unsigned int, 4> &oop_idxs, float displacement)
{
    std::shared_ptr<OpenBabel::OBMol> out(new OpenBabel::OBMol(*mol));
    double new_offset = displacement + OBMolGetOOPOffset(*mol, mol->GetAtom(oop_idxs[0]), mol->GetAtom(oop_idxs[1]),
                                                         mol->GetAtom(oop_idxs[2]), mol->GetAtom(oop_idxs[3]));

    std::ostringstream label;
    label << "OUTOFPLANE ";
    for (auto idx : oop_idxs) {
        label << idx << " ";
    }
    label << new_offset;
    out->SetTitle(label.str().c_str()); // c_str() needed as SetTitle(std::string &) is unusable

    OBMolSetOOPOffset(*out, out->GetAtom(oop_idxs[0]), out->GetAtom(oop_idxs[1]),
                    out->GetAtom(oop_idxs[2]), out->GetAtom(oop_idxs[3]), new_offset);

    return out;
}


std::pair<std::shared_ptr<OpenBabel::OBMol>, std::shared_ptr<OpenBabel::OBBond>> BATLibrary::protonate_atom(const OpenBabel::OBAtom& basic_atom)
{
    std::shared_ptr<OpenBabel::OBMol> out(new OpenBabel::OBMol(*mol));
    OpenBabel::OBAtom *base = out->GetAtom(basic_atom.GetIdx());

    OBMolSetupAllExplicit(*out);
    base->IncrementImplicitValence();
    out->AddHydrogens(base);

    // delete outdated angle list
    out->DeleteData(OpenBabel::OBGenericDataType::AngleData);

    unsigned int N = out->NumAtoms();
    OpenBabel::OBBitVec frozen_atoms(N);
    frozen_atoms.Negate();
    frozen_atoms.SetBitOff(N);

    OBMolOptimise(*out, std::move(frozen_atoms));

    std::shared_ptr<OpenBabel::OBBond> new_bond(out, out->GetBond(base, out->GetAtom(N)));

    return std::make_pair(std::move(out), std::move(new_bond));
}


std::shared_ptr<OpenBabel::OBMol> BATLibrary::displace_nbdist(const std::vector<unsigned int> &group_1, const std::vector<unsigned int> &group_2, OpenBabel::vector3 dir, double dist, float factor)
{
    std::shared_ptr<OpenBabel::OBMol> out(new OpenBabel::OBMol(*mol));

    std::ostringstream label;
    label << "NBDIST 1 2 " << dist * factor;
    out->SetTitle(label.str().c_str()); // c_str() needed as SetTitle(std::string &) is unusable

    dir *= (factor - 1) * dist / 2;

    OBMolTranslateGroup(*out, group_2, dir);
    OBMolTranslateGroup(*out, group_1, -dir);

    return out;
}


template<class IndexArray> bool BATLibrary::check_active(const IndexArray &idxs) const
{
    if (active_atoms.empty()) {
        return true;
    }

    for (auto idx : idxs) {
        if (active_atoms.count(idx) == 0) {
            return false;
        }
    }

    return true;
}


std::vector<unsigned int> BATLibrary::filter_active(const std::vector<unsigned int> &idxs) const
{
    if (active_atoms.empty()) {
        return idxs;
    }

    std::vector<unsigned int> filtered;

    for (unsigned int idx : idxs) {
        if (active_atoms.count(idx) != 0) {
            filtered.push_back(idx);
        }
    }

    return filtered;
}


}
