#!/bin/bash

for f in ../src/*.xyz; do dst=${f##*/}; ../../batlibgen -i $f -o ${dst%.*}.xyz --nbdist-factors "0.5..2"; done
