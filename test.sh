#!/bin/bash

cd test-reference

for md5 in *.md5; do
    testdir=${md5%.md5}
    echo $testdir
    cd $testdir
    ./run.sh | grep -vE "^#" > run.out
    md5sum --quiet -c ../$md5
    cd ..
done
