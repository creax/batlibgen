#ifndef FRAGLIBGEN_FRAGLIBRARY_H
#define FRAGLIBGEN_FRAGLIBRARY_H

#include <iosfwd>
#include <memory>
#include <map>

namespace OpenBabel {
class OBAtom;
class OBMol;
}

namespace FragLibGen {

class FragLibrary {
public:
    void add(std::unique_ptr<OpenBabel::OBMol> input);
    void recursive_run(void);
    void center(void);
    void sort_atoms(void);
    void write(const std::string &prefix, std::ostream &list);

protected:
    void add_molecule(std::unique_ptr<OpenBabel::OBMol> mol);
    void add_fragment(std::unique_ptr<OpenBabel::OBMol> frag);

private:
    static bool atom_before_CHfirst(OpenBabel::OBAtom *a, OpenBabel::OBAtom *b);
    std::map<std::string,std::unique_ptr<OpenBabel::OBMol>> fragments;
};

}

#endif
