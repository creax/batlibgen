#include "fraglibgen_globals.h"
#include "fraglibrary.h"

#include <openbabel/mol.h>
#include <openbabel/obconversion.h>

#include <exception>
#include <execinfo.h>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <utility>

namespace BATLibGen {
extern const char *version;
}

namespace FragLibGen {

void terminate_handler(void)
{
    void *trace_addrs[20];
    int trace_len = backtrace(trace_addrs, sizeof(trace_addrs));
    backtrace_symbols_fd(trace_addrs, trace_len, 2);

    __gnu_cxx::__verbose_terminate_handler();
}

std::unique_ptr<OpenBabel::OBMol> read_molecule(const std::string filename)
{
    OpenBabel::OBConversion conv;
    OpenBabel::OBFormat *format = conv.FormatFromExt(filename);

    if (!format || !conv.SetInFormat(format)) {
        std::ostringstream s;
        s << "Unsupported file format: " << filename;
        throw std::runtime_error(s.str());
    }

    std::ifstream str(filename);
    if (!str.good()) {
        std::ostringstream s;
        s << "Can't open input file: " << filename;
        throw std::runtime_error(s.str());
    }

    std::unique_ptr<OpenBabel::OBMol> mol(new OpenBabel::OBMol);
    conv.Read(mol.get(), &str);

    return mol;
}
}

int main(int argc, char *argv[])
{
    using namespace FragLibGen;

    std::set_terminate(terminate_handler);

    std::cout << "# FragLibGen version " << BATLibGen::version << std::endl;

    if (!globals.init_config(argc, argv)) {
        return 1;
    }

    FragLibrary lib;

    for (const std::string &inp : globals.cfg<std::vector<std::string>>("input")) {
        lib.add(read_molecule(inp));
    }
    for (unsigned int i = globals.cfg<unsigned int>("passes"); i > 0; i--) {
        lib.recursive_run();
    }

    lib.center();
    lib.sort_atoms();

    lib.write(globals.cfg<std::string>("output"), std::cout);

    return 0;
}
