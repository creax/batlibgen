CXXFLAGS ?= -Og -march=native
override CXXFLAGS += -g -Wall -Wextra -std=c++11 -pedantic -pthread
LDFLAGS := -rdynamic
batlibgen_sources := batlibrary.cpp globals.cpp main.cpp obmolextras.cpp
batlibgen_libs := -lboost_program_options
fraglibgen_sources := fraglibgen.cpp fraglibgen_globals.cpp fraglibrary.cpp obmolextras.cpp

ifdef OBCORE_PATH
    override CXXFLAGS += -I$(OBCORE_PATH)/include
    LDFLAGS += -L$(OBCORE_PATH)/lib -Wl,--rpath $(OBCORE_PATH)/lib
    batlibgen_libs += -lobcore -ldl
else
    override CXXFLAGS += -I/usr/include/openbabel-2.0
    batlibgen_libs += -lopenbabel
endif
fraglibgen_libs := $(batlibgen_libs)

ifdef BOOST_PATH
    override CXXFLAGS += -I$(BOOST_PATH)/include
    override LDFLAGS += -L$(BOOST_PATH)/lib -Wl,--rpath $(BOOST_PATH)/lib
endif

batlibgen_objs := $(batlibgen_sources:.cpp=.o)
fraglibgen_objs := $(fraglibgen_sources:.cpp=.o)
targets := batlibgen$(BUILD_SUFFIX) fraglibgen$(BUILD_SUFFIX)

batlibgen$(BUILD_SUFFIX): $(batlibgen_objs) version.o
	$(CXX) $(CXXFLAGS) $(LDFLAGS) -o batlibgen$(BUILD_SUFFIX) $(batlibgen_objs) $(batlibgen_libs) version.o

fraglibgen$(BUILD_SUFFIX): $(fraglibgen_objs) version.o
	$(CXX) $(CXXFLAGS) $(LDFLAGS) -o fraglibgen$(BUILD_SUFFIX) $(fraglibgen_objs) $(fraglibgen_libs) version.o

.PHONY: clean force all

all: $(targets)

clean:
	rm -f $(batlibgen_objs) $(fraglibgen_objs) $(targets:=.o) version.o $(targets)

version.cpp: force
	@echo "namespace BATLibGen { const char *version = \"`git describe --always --dirty`\"; }" > version.tmp
	@cmp -s version.tmp version.cpp || mv version.tmp version.cpp
	@rm -f version.tmp 2>/dev/null

%.d: %.cpp
	@set -e; rm -f $@; \
	$(CXX) $(CXXFLAGS) -MM $< > $@.$$$$; \
	sed 's,\($*\)\.o[ :]*,\1.o $@ : ,g' < $@.$$$$ > $@; \
	rm -f $@.$$$$

include $(batlibgen_sources:.cpp=.d)
include $(fraglibgen_sources:.cpp=.d)
