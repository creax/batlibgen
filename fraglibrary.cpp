#include "fraglibrary.h"
#include "obmolextras.h"

#include <openbabel/mol.h>
#include <openbabel/obconversion.h>

#include <algorithm>
#include <forward_list>
#include <ostream>
#include <iomanip>
#include <stdexcept>
#include <string>
#include <vector>

namespace FragLibGen {

void FragLibrary::add(std::unique_ptr<OpenBabel::OBMol> input)
{
    std::unique_ptr<OpenBabel::OBMol> mol(new OpenBabel::OBMol);
    OpenBabel::OBMolAtomDFSIter it(*input);
    while (input->GetNextFragment(it, *mol)) {
        add_molecule(std::move(mol));
        mol.reset(new OpenBabel::OBMol);
    }
}


void FragLibrary::recursive_run(void)
{
    std::vector<std::unique_ptr<OpenBabel::OBMol>> tmp_fragments;
    tmp_fragments.reserve(fragments.size());

    for (auto &f: fragments) {
        tmp_fragments.emplace_back(new OpenBabel::OBMol(*(f.second)));
    }

    for (auto &f: tmp_fragments) {
        add_molecule(std::move(f));
    }
}


void FragLibrary::sort_atoms(void)
{
    for (auto &f: fragments) {
        OpenBabel::OBMol &frag = *(f.second);
        std::vector<OpenBabel::OBAtom *> atoms(frag.NumAtoms());
        std::copy(frag.BeginAtoms(), frag.EndAtoms(), atoms.begin());
        std::stable_sort(atoms.begin(), atoms.end(), atom_before_CHfirst);
        frag.RenumberAtoms(atoms);
    }
}


void FragLibrary::center(void)
{
    for (auto &f: fragments) {
        f.second->ToInertialFrame();
    }
}


void FragLibrary::write(const std::string &prefix, std::ostream &list)
{
    typedef decltype(fragments.begin()) frag_iter;
    std::forward_list<std::tuple<std::string, frag_iter, unsigned int>> formula_ranges;

    for (auto itf = fragments.begin(); itf != fragments.end();) {
        formula_ranges.emplace_front(itf->first.substr(0, itf->first.find('/') + 1), itf, 1);

        const std::string &cur_formula = std::get<0>(formula_ranges.front());
        ++itf;
        while ((itf != fragments.end()) && (itf->first.compare(0, cur_formula.length(), cur_formula) == 0)) {
            std::get<2>(formula_ranges.front())++;
            ++itf;
        }
    }

    OpenBabel::OBConversion out_conv;
    out_conv.SetOutFormat("xyz");

    unsigned int nfrag = 0, nframes = 0, nat = 0;
    for (auto &f: formula_ranges) {
        auto itf = std::get<1>(f);
        unsigned int N = std::get<2>(f);
        std::string formula = std::move(std::get<0>(f));
        formula.pop_back();

        nfrag++;
        nframes += N;

        unsigned int width = 1;
        while (N >= 10) {
            N /= 10;
            width++;
        }
        N = std::get<2>(f);
        for (unsigned int i = 0; i < N; i++, itf++) {
            std::ostringstream ofn;
            ofn << prefix << formula << '.' << std::setfill('0') << std::setw(width) << i << ".xyz";
            std::ofstream ofile(ofn.str());
            out_conv.Write(itf->second.get(), &ofile);
            list << ofn.str() << '\n';
            nat += itf->second->NumAtoms();
        }
    }

    list << "# " << nfrag << " targets, " << nframes << " frames, " << nat << " atoms\n";
}


/*
 * The idea of this version was to inherit the valence info from the unbroken molecule
 * and thus avoid (potentially unreliable) re-perceiving in the fragments.
 * However, it doesn't work that way because OBMol's copy constructor does not copy
 * any of the extra info (implicit valences, bond orders etc.)
 *
 * Let's just use the simpler version with GetNextFragment/PerceiveBondOrders and
 * hope it works well enough.
void FragLibrary::add_molecule(std::unique_ptr<OpenBabel::OBMol> mol)
{
    for (OpenBabel::OBMolBondIter it(*mol); it; ++it) {
        if (it->IsInRing()) {
            continue;
        }

        OpenBabel::OBMol tmp(*mol);
        tmp.DeleteBond(tmp.GetBond(it->GetIdx()));

        OpenBabel::OBMolAtomDFSIter itf(tmp);
        std::vector<unsigned int> frag_ids(tmp.NumAtoms());
        unsigned int nfrag;
        for (nfrag = 0; itf; ++nfrag) {
            do { //for each atom in fragment
                frag_ids[itf->GetIdx() - 1] = nfrag;
            }while((itf++).next());
        }

        for (unsigned int i = 0; i < nfrag; ++i) {
            std::unique_ptr<OpenBabel::OBMol> frag(new OpenBabel::OBMol(*mol));
            frag->BeginModify();
            for (unsigned int ia = frag->NumAtoms(); ia > 0; ia--) {
                if (frag_ids[frag->GetAtom(ia)->GetIdx() - 1] != i) {
                    frag->DeleteAtom(frag->GetAtom(ia));
                }
            }
            frag->EndModify();
            frag->PerceiveBondOrders();

            add_fragment(std::move(frag));
        }
    }
}*/
void FragLibrary::add_molecule(std::unique_ptr<OpenBabel::OBMol> mol)
{
    for (OpenBabel::OBMolBondIter it(*mol); it; ++it) {
        if (it->IsInRing()) {
            continue;
        }

        OpenBabel::OBMol tmp(*mol);
        tmp.DeleteBond(tmp.GetBond(it->GetIdx()));

        OpenBabel::OBMolAtomDFSIter itf(tmp);
        std::unique_ptr<OpenBabel::OBMol> frag(new OpenBabel::OBMol);
        while (tmp.GetNextFragment(itf, *frag)) {
            // make sure frag has coordinate/conformer data
            frag->BeginModify();
            frag->EndModify(false);

            frag->PerceiveBondOrders();

            /* The total spin is not valid, but AssignSpinMultiplicity() marks it so.
             * Let's unmark it to make GetTotalSpinMultiplicity() work. */
            frag->UnsetFlag(OB_TSPIN_MOL);

            add_fragment(std::move(frag));
            frag.reset(new OpenBabel::OBMol);
        }
    }
}


void FragLibrary::add_fragment(std::unique_ptr<OpenBabel::OBMol> frag)
{
    if (frag->NumAtoms() == 1) {
        return;
    }

/*    std::cout << frag->GetFormula() << ' ' << frag->GetTotalSpinMultiplicity() << std::endl;
    for (OpenBabel::OBMolAtomIter it(*frag); it; ++it) {
        std::cout << ' ' << it->GetAtomicNum()
                  << ' ' << it->GetImplicitValence()
                  << ' ' << it->GetValence()
                  << ' ' << it->GetSpinMultiplicity()
                  << std::endl;
    }
*/
    OpenBabel::obMessageLevel log_level = OpenBabel::obErrorLog.GetOutputLevel();
    OpenBabel::obErrorLog.SetOutputLevel(OpenBabel::obMessageLevel::obError);
    OpenBabel::OBConversion conv;
    conv.SetOutFormat("inchi");
    std::string inchi = conv.WriteString(frag.get(), true);
    OpenBabel::obErrorLog.SetOutputLevel(log_level);

    frag->SetTitle(inchi);

    if ((inchi.compare(0, 9, "InChI=1S/") != 0) || (inchi.find('/', 9) == std::string::npos)) {
        throw std::logic_error("FragLibrary::add_fragment: unexpected InChI format returned by OpenBabel");
    }
    inchi.erase(0, 9);

    fragments.emplace(std::move(inchi), std::move(frag));
}


bool FragLibrary::atom_before_CHfirst(OpenBabel::OBAtom* a, OpenBabel::OBAtom* b)
{
    int na = a->GetAtomicNum(), nb = b->GetAtomicNum();
    if (na == 6) {
        return (nb != 6);
    }
    if (na == 1) {
        return (nb != 6) && (nb != 1);
    }
    return na < nb;
}


}
