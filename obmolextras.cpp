#include "obmolextras.h"

#include <openbabel/forcefield.h>
#include <openbabel/math/matrix3x3.h>

#include <climits>
#include <cmath>
#include <iostream>

namespace BATLibGen {

void OBUniqueTorsionIter::findTorsion(void)
{
    OpenBabel::OBAtom *a, *b, *c, *d;
    bool found = false;

    for (; bond; ++bond) {
        b = bond->GetBeginAtom();
        c = bond->GetEndAtom();
        OpenBabel::OBBondIterator it_nbr;

        for (a = b->BeginNbrAtom(it_nbr); a != nullptr; a = b->NextNbrAtom(it_nbr)) {
            if (a != c) {
                break;
            }
        }
        if (a == nullptr) {
            continue; // atom A has no usable neighbors, try another B-C pair
        }

        for (d = c->BeginNbrAtom(it_nbr); d != nullptr; d = c->NextNbrAtom(it_nbr)) {
            if (d != b) {
                break;
            }
        }
        if (d != nullptr) {
            found = true;
            break; // usable neighbor of D found => done
        }
    }

    if (found) {
        indices[0] = a->GetIdx();
        indices[1] = b->GetIdx();
        indices[2] = c->GetIdx();
        indices[3] = d->GetIdx();
    }
}


void OBPlanarCenterIter::findPlanarCenter(void)
{
    for (; center; ++center) {
        OpenBabel::OBAtom *a, *b, *c, *d;

        a = &(*center);
        if (a->GetValence() != 3) {
            continue;
        }
        OpenBabel::OBBondIterator it_nbr;
        b = a->BeginNbrAtom(it_nbr);
        c = a->NextNbrAtom(it_nbr);
        d = a->NextNbrAtom(it_nbr);

        if (std::abs(OBMolGetOOPOffset(*parent, a, b, c, d)) <= threshold) {
            indices[0] = a->GetIdx();
            indices[1] = b->GetIdx();
            indices[2] = c->GetIdx();
            indices[3] = d->GetIdx();
            break;
        }
    }
}


void OBBasicAtomIter::findBasicAtom(void)
{
    for (; basic_atom; ++basic_atom) {
        int nlp = basic_atom->GetHyb() + 1 - basic_atom->GetImplicitValence();
        if (nlp >= 1) {
            return;
        }
    }
}


void OBMolSetAngle(OpenBabel::OBMol& mol, OpenBabel::OBAtom* a, OpenBabel::OBAtom* b, OpenBabel::OBAtom* c, double angle)
{
    std::vector<OpenBabel::OBAtom *> atoms_to_move;

    mol.FindChildren(atoms_to_move, b, c);
    atoms_to_move.push_back(c);

    double orig_angle = a->GetAngle(b, c);
    OpenBabel::matrix3x3 R;

    OpenBabel::vector3 va, vc, axis;
    const OpenBabel::vector3 &origin = b->GetVector();
    va = a->GetVector() - origin;
    vc = c->GetVector() - origin;
    axis = cross(vc, va);
    if (axis.length_2() <= 1e-6) {
        va.createOrthoVector(axis);
    }

    angle -= orig_angle;
    if (dot(vc, cross(va, axis)) < 0) {
        angle = -angle;
    }

    R.RotAboutAxisByAngle(axis, angle);

    for (OpenBabel::OBAtom *at : atoms_to_move) {
        at->SetVector(origin + (R * (at->GetVector() - origin)));
    }
}


void OBMolSetAngleSymmetric(OpenBabel::OBMol& mol, OpenBabel::OBAtom* a, OpenBabel::OBAtom* b, OpenBabel::OBAtom* c, double angle)
{
    double orig_angle = a->GetAngle(b, c);
    OBMolSetAngle(mol, a, b, c, (angle + orig_angle) / 2);
    OBMolSetAngle(mol, c, b, a, angle);
}


double OBMolGetOOPOffset(OpenBabel::OBMol& mol, OpenBabel::OBAtom* a, OpenBabel::OBAtom* b, OpenBabel::OBAtom* c, OpenBabel::OBAtom* d)
{
    (void) mol;
    return OpenBabel::Point2PlaneSigned(a->GetVector(), b->GetVector(), c->GetVector(), d->GetVector());
}


void OBMolSetOOPOffset(OpenBabel::OBMol& mol, OpenBabel::OBAtom* a, OpenBabel::OBAtom* b, OpenBabel::OBAtom* c, OpenBabel::OBAtom* d, double offset)
{
    (void) mol;
    const OpenBabel::vector3 &vb = b->GetVector(), &vc = c->GetVector(), &vd = d->GetVector();
    OpenBabel::vector3 v_norm, va = a->GetVector();
    v_norm = cross((vc-vb), (vd-vb)).normalize();

    double orig_offset = dot((va - vb), v_norm);
    va += (offset - orig_offset) * v_norm;
    a->SetVector(va);
}


double OBMolMinPairDistance(OpenBabel::OBMol &mol, bool exclude_bonded)
{
    double min_dist = INFINITY;

    OpenBabel::OBAtomIterator end = mol.EndAtoms();
    for (OpenBabel::OBAtomIterator it1 = mol.BeginAtoms(); it1 != end; ++it1) {
        for (OpenBabel::OBAtomIterator it2 = mol.BeginAtoms(); it2 != it1; ++it2) {
            if (exclude_bonded) {
                OpenBabel::OBBondIterator it_nbr;
                bool bonded = false;
                for (OpenBabel::OBAtom *nbr = (*it1)->BeginNbrAtom(it_nbr); nbr != nullptr; nbr = (*it1)->NextNbrAtom(it_nbr)) {
                    if (nbr == *it2) {
                        bonded = true;
                        break;
                    }
                }
                if (bonded) {
                    continue;
                }
            }

            double dist = (*it1)->GetDistance(*it2);
            if (dist < min_dist) {
                min_dist = dist;
            }
        }
    }

    return min_dist;
}


void OBMolSetupAllExplicit(OpenBabel::OBMol &mol)
{
    // assume all hydrogens are explicitly present
    for (OpenBabel::OBMolAtomIter it(mol); it; ++it) {
        it->SetImplicitValence(it->GetValence());
    }
    mol.SetImplicitValencePerceived();
}


void OBMolOptimise(OpenBabel::OBMol &mol, OpenBabel::OBBitVec frozen_atoms)
{
    OpenBabel::OBFFConstraints constraints;
    for (OpenBabel::OBMolAtomIter it(mol); it; ++it) {
        unsigned int i = it->GetIdx();
        if (frozen_atoms.BitIsOn(i)) {
            constraints.AddAtomConstraint(i);
        }
    }

    OpenBabel::OBForceField *ff = OpenBabel::OBForceField::FindForceField("MMFF94");
    if (ff == nullptr) {
        throw std::runtime_error("OBMolOptimise: Error loading force field");
    }

    ff->SetLogFile(&std::cerr);
    ff->SetLogLevel(OBFF_LOGLVL_NONE);
    if (!ff->Setup(mol, constraints)) {
        throw std::runtime_error("OBMolOptimise: Error setting up force field");
    }

    ff->ConjugateGradients(10000);
    ff->GetCoordinates(mol);
}


std::vector<std::vector<unsigned int>> OBMolFindMonomers(OpenBabel::OBMol &mol)
{
    std::vector<std::vector<unsigned int>> monomers;

    OpenBabel::OBMolAtomDFSIter it(mol);
    while (it) {
        std::vector<unsigned int> m;
        do {
            m.push_back(it->GetIdx());
            if (!it.next()) {
                ++it;
                break;
            }
        } while (++it);
        monomers.push_back(std::move(m));
    }

    return monomers;
}


std::vector<std::tuple<unsigned int, unsigned int, double>> OBMolNonbondedNeighbors(
    OpenBabel::OBMol &mol, const std::vector<unsigned int> &group1,
    const std::vector<unsigned int> &group2, double &mindist, double cutoff)
{
    mindist = INFINITY;
    std::vector<std::tuple<unsigned int, unsigned int, double>> pairs;
    for (unsigned int i : group1) {
        OpenBabel::OBAtom *ia = mol.GetAtom(i);
        for (unsigned int j : group2) {
            double d = ia->GetDistance(j);
            if (d > mindist + cutoff) {
                continue;
            }
            if (d < mindist) {
                mindist = d;
            }

            pairs.emplace_back(i, j, d);
        }
    }

    return pairs;
}



OpenBabel::vector3 OBMolGroupCenter(OpenBabel::OBMol &mol, const std::vector<unsigned int> &idxs)
{
    OpenBabel::vector3 c;

    for (unsigned int idx : idxs) {
        c += mol.GetAtom(idx)->GetVector();
    }

    c /= idxs.size();
    return c;
}


OpenBabel::vector3 OBMolGroupWeightedCenter(OpenBabel::OBMol &mol,
                                            const std::vector<unsigned int> &idxs,
                                            const std::vector<double> &weights)
{
    OpenBabel::vector3 c;
    double wsum = 0.0;

    for (unsigned int idx : idxs) {
        c += weights[idx] * mol.GetAtom(idx)->GetVector();
        wsum += weights[idx];
    }

    c /= wsum;
    return c;
}


OpenBabel::vector3 OBMolGroupCOM(OpenBabel::OBMol &mol, const std::vector<unsigned int> &idxs)
{
    OpenBabel::vector3 com;
    double mass = 0.0;

    for (unsigned int idx : idxs) {
        OpenBabel::OBAtom *at = mol.GetAtom(idx);
        double m = at->GetAtomicMass();
        com += m * at->GetVector();
        mass += m;
    }

    com /= mass;
    return com;
}


void OBMolTranslateGroup(OpenBabel::OBMol& mol, const std::vector<unsigned int> &idxs, const OpenBabel::vector3& v)
{
    for (unsigned int idx : idxs) {
        OpenBabel::OBAtom *at = mol.GetAtom(idx);
        at->SetVector(at->GetVector() + v);
    }
}


}
