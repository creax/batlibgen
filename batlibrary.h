#ifndef BATLIBGEN_BATLIBRARY_H
#define BATLIBGEN_BATLIBRARY_H

#include <openbabel/mol.h>

#include <memory>
#include <vector>
#include <set>
#include <utility>

namespace BATLibGen {

class BATLibrary : public std::vector<std::shared_ptr<OpenBabel::OBMol>> {
public:
    BATLibrary(std::unique_ptr<OpenBabel::OBMol> &&input_mol);
    void set_active(const std::set<unsigned int> &active)
    {
        active_atoms = active;
    }

    void generate_bond_structures(void);
    void generate_angle_structures(void);
    void generate_torsion_structures(void);
    void generate_outofplane_structures(void);
    void generate_protonated_structures(void);
    void generate_nbdist_structures(void);

    void align(void);
    void remove_duplicates(void);
    void remove_clashes(void);

protected:
    double normalize_angle(double x)
    {
        while (x >= 360.0) {
            x -= 360.0;
        }
        return x;
    }

    std::shared_ptr<OpenBabel::OBMol> displace_bond(const OpenBabel::OBBond &bond, float factor)
    {
        return displace_bond(*mol, bond, factor);
    }

    static std::shared_ptr<OpenBabel::OBMol> displace_bond(const OpenBabel::OBMol &mol, const OpenBabel::OBBond &bond, float factor);
    std::shared_ptr<OpenBabel::OBMol> displace_angle(const std::array<unsigned int, 3> &angle_idxs, float factor);
    std::shared_ptr<OpenBabel::OBMol> displace_torsion(const std::array<unsigned int, 4> &tors_idxs, float x);
    std::shared_ptr<OpenBabel::OBMol> displace_outofplane(const std::array<unsigned int, 4> &oop_idxs, float displacement);
    std::pair<std::shared_ptr<OpenBabel::OBMol>, std::shared_ptr<OpenBabel::OBBond>> protonate_atom(const OpenBabel::OBAtom &basic_atom);
    std::shared_ptr<OpenBabel::OBMol> displace_nbdist(const std::vector<unsigned int> &group_1, const std::vector<unsigned int> &group_2, OpenBabel::vector3 dir, double dist, float factor);

    template<class IndexArray> bool check_active(const IndexArray &idxs) const;
    std::vector<unsigned int> filter_active(const std::vector<unsigned int> &idxs) const;

    std::unique_ptr<OpenBabel::OBMol> mol;
    std::set<unsigned int> active_atoms;
};

}

#endif
