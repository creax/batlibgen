#ifndef BATLIBGEN_OBMOLEXTRAS_H
#define BATLIBGEN_OBMOLEXTRAS_H

#include <openbabel/mol.h>
#include <openbabel/bitvec.h>
#include <openbabel/obiter.h>

#include <array>
#include <vector>

namespace BATLibGen {

class OBUniqueTorsionIter
{
public:
    OBUniqueTorsionIter()
    {
    }
    OBUniqueTorsionIter(OpenBabel::OBMol *mol) : parent(mol), bond(mol)
    {
        findTorsion();
    }
    OBUniqueTorsionIter(OpenBabel::OBMol &mol) : parent(&mol), bond(mol)
    {
        findTorsion();
    }
    ~OBUniqueTorsionIter()
    {
    }

    operator bool() const
    {
        return static_cast<bool>(bond);
    }
    OBUniqueTorsionIter& operator++()
    {
        ++bond;
        findTorsion();
        return *this;
    }
    std::array<unsigned int, 4> operator*() const
    {
        return indices;
    }
protected:
    void findTorsion(void);
    OpenBabel::OBMol *parent;
    OpenBabel::OBMolBondIter bond;
    std::array<unsigned int, 4> indices;
};


class OBPlanarCenterIter
{
public:
    OBPlanarCenterIter(float threshold = 0.1) : threshold(threshold)
    {
    }
    OBPlanarCenterIter(OpenBabel::OBMol *mol, float threshold = 0.1) : parent(mol), center(mol), threshold(threshold)
    {
        findPlanarCenter();
    }
    OBPlanarCenterIter(OpenBabel::OBMol &mol, float threshold = 0.1) : parent(&mol), center(mol), threshold(threshold)
    {
        findPlanarCenter();
    }
    ~OBPlanarCenterIter()
    {
    }

    operator bool() const
    {
        return static_cast<bool>(center);
    }
    OBPlanarCenterIter& operator++()
    {
        ++center;
        findPlanarCenter();
        return *this;
    }
    std::array<unsigned int, 4> operator*() const
    {
        return indices;
    }
protected:
    void findPlanarCenter(void);
    OpenBabel::OBMol *parent;
    OpenBabel::OBMolAtomIter center;
    std::array<unsigned int, 4> indices;
    float threshold;
};


class OBBasicAtomIter
{
public:
    OBBasicAtomIter(void)
    {
    }
    OBBasicAtomIter(OpenBabel::OBMol *mol) : parent(mol), basic_atom(mol)
    {
        findBasicAtom();
    }
    OBBasicAtomIter(OpenBabel::OBMol &mol) : parent(&mol), basic_atom(mol)
    {
        findBasicAtom();
    }
    ~OBBasicAtomIter()
    {
    }

    operator bool() const
    {
        return static_cast<bool>(basic_atom);
    }
    OBBasicAtomIter& operator++()
    {
        ++basic_atom;
        findBasicAtom();
        return *this;
    }

    OpenBabel::OBAtom &operator*() const
    {
        return *basic_atom;
    }
    OpenBabel::OBAtom *operator->() const
    {
        return &(*basic_atom);
    }
protected:
    void findBasicAtom(void);
    OpenBabel::OBMol *parent;
    OpenBabel::OBMolAtomIter basic_atom;
};


void OBMolSetAngle(OpenBabel::OBMol &mol, OpenBabel::OBAtom *a, OpenBabel::OBAtom *b, OpenBabel::OBAtom *c, double angle);
void OBMolSetAngleSymmetric(OpenBabel::OBMol &mol, OpenBabel::OBAtom *a, OpenBabel::OBAtom *b, OpenBabel::OBAtom *c, double angle);

double OBMolGetOOPOffset(OpenBabel::OBMol &mol, OpenBabel::OBAtom *a, OpenBabel::OBAtom *b, OpenBabel::OBAtom *c, OpenBabel::OBAtom *d);
void OBMolSetOOPOffset(OpenBabel::OBMol &mol, OpenBabel::OBAtom *a, OpenBabel::OBAtom *b, OpenBabel::OBAtom *c, OpenBabel::OBAtom *d, double offset);

double OBMolMinPairDistance(OpenBabel::OBMol& mol, bool exclude_bonded = false);

void OBMolSetupAllExplicit(OpenBabel::OBMol &mol);

void OBMolOptimise(OpenBabel::OBMol &mol, OpenBabel::OBBitVec frozen_atoms = OpenBabel::OBBitVec());

std::vector<std::vector<unsigned int>> OBMolFindMonomers(OpenBabel::OBMol &mol);
std::vector<std::tuple<unsigned int, unsigned int, double>> OBMolNonbondedNeighbors(OpenBabel::OBMol &mol, const std::vector<unsigned int> &group1, const std::vector<unsigned int> &group2, double &mindist, double cutoff = INFINITY);
OpenBabel::vector3 OBMolGroupCenter(OpenBabel::OBMol &mol, const std::vector<unsigned int> &idxs);
OpenBabel::vector3 OBMolGroupWeightedCenter(OpenBabel::OBMol &mol, const std::vector<unsigned int> &idxs, const std::vector<double> &weights);
OpenBabel::vector3 OBMolGroupCOM(OpenBabel::OBMol &mol, const std::vector<unsigned int> &idxs);
void OBMolTranslateGroup(OpenBabel::OBMol &mol, const std::vector<unsigned int> &idxs, const OpenBabel::vector3 &v);

}

#endif
