# BATLibGen: Generate perturbed geometries for force field training

This project consists of two utilities to generate libraries of perturbed molecular geometries for a given molecule:

- batlibgen: Perform rigid scans of intramolecular degrees of freedom (bond stretching, angle bending, torsion, out-of-plane motions) as well as non-covalent intermolecular interactions (e.g. hydrogen-bonded dimers). This can also generate protonated structures by adding a proton to each potentially basic group in the molecule in turn.
- fraglibgen: Generate libraries of molecular fragments by systematically splitting bonds, discarding single-atom fragments, and optionally recursively re-fragmenting the results.

A detailed description of the approach is available in the following paper (consider citing it if you use these tools):

Trnka, T., Tvaroška, I., Koča, J. Automated Training of ReaxFF Reactive Force Fields for Energetics of Enzymatic Reactions. _Journal of Chemical Theory and Computation_ **2018** 14 (1), 291-302, https://doi.org/10.1021/acs.jctc.7b00870

## Installation

You will need the following dependencies to build these tools:

- OpenBabel 2.x (the code has not been ported to 3.x yet, patches are welcome)
- Boost program_options library

Building is as simple as running `make`. You can customize some aspects of the build by passing variables on the `make` command line, for example `make CXXFLAGS="-O3 -march=native" BOOST_PATH="/path/to/boost"`.

## Usage

Run `./batlibgen --help` or `./fraglibgen --help` for a list of available options.
