#ifndef BATLIBGEN_GLOBALS_H
#define BATLIBGEN_GLOBALS_H

#include <boost/program_options.hpp>

namespace BATLibGen
{

class Globals
{
public:
    Globals();

    bool init_config(int argc, char *argv[]);

    template<typename T> const T &cfg(const std::string &name) const {
        return optmap[name].as<T>();
    }

private:
    boost::program_options::options_description opts;
    boost::program_options::variables_map optmap;
    void print_help(const boost::program_options::options_description &opt_desc) const;
};

extern Globals globals;

}
#endif
